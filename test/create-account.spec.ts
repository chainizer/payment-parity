import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {expect} from 'chai';
import {generate} from 'ethereumjs-wallet';
import 'mocha';
import {Parity, parity} from '../src/parity';
import '../src/commands';
import {CREATE_ACCOUNT, CreateAccountCmd, CreateAccountCmdRes} from '@chainizer/payment-api';

describe('create-account', () => {
    let p: Parity;

    before(() => {
        p = parity();
    });

    it('should create an account', async () => {
        const payload: CreateAccountCmd = {};
        const metadata: Metadata = {};
        const cmdRes = <CreateAccountCmdRes> await executeTask(TaskType.command, CREATE_ACCOUNT, payload, metadata);
        expect(cmdRes.address).to.exist;
        expect(cmdRes.privateKey).to.exist;
    });

});
