import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {WITHDRAW_ACCOUNT, WithdrawAccountCmd, WithdrawAccountCmdRes} from '@chainizer/payment-api';
import {expect} from 'chai';
import {generate} from 'ethereumjs-wallet';
import 'mocha';
import {Parity, parity} from '../src/parity';
import '../src/commands';
import {config} from '../src/config';

async function send(p: Parity, value: string) {
    const coinbase = await p.getCoinbase();
    const wallet1 = generate();
    await p.unlockAccount(coinbase, '');
    await p.sendTransaction({
        from: coinbase,
        to: wallet1.getAddressString(),
        value
    });
    return wallet1;
}

describe('withdraw-account', () => {
    let p: Parity;

    before(() => {
        p = parity();
    });

    it('should withdraw the whole balance when amount < min', async () => {
        const wallet1 = await send(p, p.api.util.toWei(config().business.min, 'ether'));
        const wallet2 = generate();
        const payload: WithdrawAccountCmd = {
            privateKey: wallet1.getPrivateKeyString(),
            to: wallet2.getAddressString()
        };
        const metadata: Metadata = {};

        const cmdRes = <WithdrawAccountCmdRes> await executeTask(TaskType.command, WITHDRAW_ACCOUNT, payload, metadata);
        expect(cmdRes.feeTx).to.not.exist;
        expect(cmdRes.userTx).to.exist;

        const balance2 = await p.getBalance(wallet2.getAddressString());
        expect(balance2.toString()).to.be.eq('997183950799000');
    });

    it('should withdraw a part of the balance when amount > min and < max', async () => {
        const wallet1 = await send(p, p.api.util.toWei('2', 'ether'));
        const wallet2 = generate();
        const coinbase = await p.getCoinbase();
        const payload: WithdrawAccountCmd = {
            privateKey: wallet1.getPrivateKeyString(),
            to: wallet2.getAddressString()
        };
        const balanceCoinbaseBefore = await p.getBalance(coinbase);
        const metadata: Metadata = {};

        const cmdRes = <WithdrawAccountCmdRes> await executeTask(TaskType.command, WITHDRAW_ACCOUNT, payload, metadata);
        expect(cmdRes.feeTx).to.exist;
        expect(cmdRes.userTx).to.exist;

        const balance2 = await p.getBalance(wallet2.getAddressString());
        expect(balance2.toString()).to.be.eq('1899994367901598000');

        const balanceCoinbaseAfter = await p.getBalance(coinbase);
        expect(balanceCoinbaseAfter.minus(balanceCoinbaseBefore).toString()).to.be.eq('100000000000000000');
    });

    it('should withdraw a part of the balance when amount > max', async () => {
        const wallet1 = await send(p, p.api.util.toWei('60', 'ether'));
        const wallet2 = generate();
        const coinbase = await p.getCoinbase();
        const payload: WithdrawAccountCmd = {
            privateKey: wallet1.getPrivateKeyString(),
            to: wallet2.getAddressString()
        };
        const balanceCoinbaseBefore = await p.getBalance(coinbase);
        const metadata: Metadata = {};

        const cmdRes = <WithdrawAccountCmdRes> await executeTask(TaskType.command, WITHDRAW_ACCOUNT, payload, metadata);
        expect(cmdRes.feeTx).to.exist;
        expect(cmdRes.userTx).to.exist;

        const balanceCoinbaseAfter = await p.getBalance(coinbase);
        expect(balanceCoinbaseAfter.minus(balanceCoinbaseBefore).toString()).to.be.eq('250000000000000000');

        const balance2 = await p.getBalance(wallet2.getAddressString());
        expect(balance2.toString()).to.be.eq('59749994367901598000');
    });

    it('should failed when not enough fund', async () => {
        const wallet1 = generate();
        const wallet2 = generate();
        const coinbase = await p.getCoinbase();
        const payload: WithdrawAccountCmd = {
            privateKey: wallet1.getPrivateKeyString(),
            to: wallet2.getAddressString()
        };
        const balanceCoinbaseBefore = await p.getBalance(coinbase);
        const metadata: Metadata = {};

        try {
            const cmdRes = <WithdrawAccountCmdRes> await executeTask(TaskType.command, WITHDRAW_ACCOUNT, payload, metadata);
            expect(cmdRes).to.not.exist;
        } catch (e) {
            expect(e.status).to.be.equals(400);
            expect(e.name).to.be.equals('NotEnoughCoins');
            expect(e.message).to.be.equals('Not enough coins to create a transaction: 0 wei');
        }
    });

});
