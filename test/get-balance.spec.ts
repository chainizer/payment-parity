import {executeTask, Metadata, TaskType} from '@chainizer/support-executor';
import {expect} from 'chai';
import {generate} from 'ethereumjs-wallet';
import 'mocha';
import {Parity, parity} from '../src/parity';
import '../src/queries';
import {GET_BALANCE, GetBalanceQry, GetBalanceQryRes} from '@chainizer/payment-api';

describe('get-balance', () => {
    let p: Parity;

    before(() => {
        p = parity();
    });

    it('should fetch none zero balance', async () => {
        const wallet1 = generate();
        const coinbase = await p.getCoinbase();
        await p.unlockAccount(coinbase, '');
        await p.sendTransaction({
            from: coinbase,
            to: wallet1.getAddressString(),
            value: p.api.util.toWei('5', 'ether')
        });
        const payload: GetBalanceQry = {
            address: wallet1.getAddressString()
        };
        const metadata: Metadata = {};
        const r = <GetBalanceQryRes> await executeTask(TaskType.query, GET_BALANCE, payload, metadata);
        expect(r.balance).to.equals('5000000000000000000');
    });

    it('should fetch zero balance', async () => {
        const address = generate().getAddressString();
        const payload: GetBalanceQry = {address};
        const metadata: Metadata = {};
        const r = <GetBalanceQryRes> await executeTask(TaskType.query, GET_BALANCE, payload, metadata);
        expect(r.balance).to.equals('0');
    });

});

