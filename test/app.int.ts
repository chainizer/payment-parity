import {expect} from 'chai';
import 'mocha';
import {getHooks, getServer} from '@chainizer/support-app';
import {config} from '../src/config';
import {getCnqAmqp} from '@chainizer/support-cnq/lib/amqp';
import {dispatchTask} from '@chainizer/support-cnq';
import {TaskType} from '@chainizer/support-executor';
import {CREATE_ACCOUNT, GET_BALANCE, WITHDRAW_ACCOUNT} from '@chainizer/payment-api';
import '../src/main';

const wait = (ms) => (new Promise(resolve => setTimeout(resolve, ms)));

config().set('cnq.producer.amqp.command.ethereum', {
    exchange: 'payment.ethereum',
    routingKey: 'command'
});

config().set('cnq.producer.amqp.query.ethereum', {
    exchange: 'payment.ethereum',
    routingKey: 'query'
});

describe('app', () => {

    it('should check health and stop the app', async () => {
        await wait(100);

        let healthError;
        try {
            getHooks().health.forEach(async hook => await hook());
        } catch (e) {
            healthError = e;
        }

        const amqp = await getCnqAmqp();
        await amqp.channel.checkExchange('payment.ethereum');
        await amqp.channel.checkQueue('payment.ethereum.command_backlog');
        await amqp.channel.checkQueue('payment.ethereum.command_invalid');
        await amqp.channel.checkQueue('payment.ethereum.query_backlog');
        await amqp.channel.checkQueue('payment.ethereum.query_invalid');

        try {
            const r = await dispatchTask('ethereum', TaskType.command, CREATE_ACCOUNT, {}, {});
            expect(r).to.not.exist;
        } catch (e) {
            expect(e).to.exist;
        }

        try {
            const r = await dispatchTask('ethereum', TaskType.command, WITHDRAW_ACCOUNT, {}, {});
            expect(r).to.not.exist;
        } catch (e) {
            expect(e).to.exist;
        }

        try {
            const r = await dispatchTask('ethereum', TaskType.query, GET_BALANCE, {}, {});
            expect(r).to.not.exist;
        } catch (e) {
            expect(e).to.exist;
        }

        await wait(100);

        let shutdownError;
        try {
            getHooks().shutdown.forEach(async hook => await hook());
        } catch (e) {
            shutdownError = e;
        }

        getServer().close();
        expect(healthError, 'health hooks failed').to.not.exist;
        expect(shutdownError, 'shutdown hooks failed').to.not.exist;
    });

});
