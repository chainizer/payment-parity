import {logger} from '@chainizer/support-winston';
import {HookType, registerHook, startApp} from '@chainizer/support-app';
import '@chainizer/support-cnq';
import './commands';
import './queries';
import {parity} from './parity';

startApp().catch(error => logger().error('App unable to start!', error));

registerHook(HookType.health, async () => {
    await parity().getCoinbase();
});