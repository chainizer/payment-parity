import {Config, config as oConfig} from '@chainizer/support-config';

export interface CustomConfig extends Config {
    parity: {
        url: string,
        pollingTimeout: number
    }
    business: {
        min: string,
        max: string,
        fee: number
    }

}

export function config(argv?: string | string[]): CustomConfig {
    return <CustomConfig> oConfig({argv});
}
