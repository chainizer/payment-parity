import {Metadata, registerTask, TaskType} from '@chainizer/support-executor';
import {parity} from './parity';
import {GET_BALANCE, GetBalanceQry, GetBalanceQryRes} from '@chainizer/payment-api';

registerTask(TaskType.query, GET_BALANCE, async (payload: GetBalanceQry, metadata: Metadata): Promise<GetBalanceQryRes> => {
    const balance = await parity().getBalance(payload.address);
    return {balance: balance.toString()};
});
