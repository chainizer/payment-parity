import {Metadata, registerTask, taskLogger, TaskType} from '@chainizer/support-executor';
import {logger} from '@chainizer/support-winston';
import {bufferToHex, privateToAddress} from 'ethereumjs-util';
import {generate} from 'ethereumjs-wallet';
import {v4 as uuid} from 'uuid';
import {config} from './config';
import {parity} from './parity';
import {
    CREATE_ACCOUNT,
    CreateAccountCmd,
    CreateAccountCmdRes,
    NotEnoughCoins,
    WITHDRAW_ACCOUNT,
    WithdrawAccountCmd,
    WithdrawAccountCmdRes
} from '@chainizer/payment-api';

registerTask(TaskType.command, CREATE_ACCOUNT, async (payload: CreateAccountCmd, metadata: Metadata): Promise<CreateAccountCmdRes> => {
    const wallet = generate();
    const address = <string> wallet.getAddressString();
    const privateKey = wallet.getPrivateKeyString();
    return {privateKey, address};
});

registerTask(TaskType.command, WITHDRAW_ACCOUNT, async (payload: WithdrawAccountCmd, metadata: Metadata): Promise<WithdrawAccountCmdRes> => {
    const p = parity();
    const l = taskLogger(metadata);

    const privateKey = payload.privateKey;
    const from = <string> bufferToHex(privateToAddress(privateKey));
    const to = payload.to;
    const password = uuid();

    l.info('withdraw from [%s] to [%s]', from, to);

    let feeRate = config().business.fee;
    const coinbase = await p.getCoinbase();
    const minBalance = p.api.util.toWei(config().business.min, 'ether');
    const maxBalance = p.api.util.toWei(config().business.max, 'ether');

    try {
        const balance = await p.getBalance(from);
        if (balance.lte(0)) {
            throw new NotEnoughCoins(balance.toString(), 'wei');
        }

        const gasPrice = await p.getGasPrice();

        if (balance.lte(minBalance)) {
            l.info('minimum balance not reached, transfer [%s] wei', balance.toString());
            await p.importAccount(privateKey, password);
            await p.unlockAccount(from, password);
            const userGas = await p.estimateGas({
                from, to, gasPrice, value: balance
            });
            const userGasAmount = userGas.mul(gasPrice);
            const userAmount = balance.minus(userGasAmount);
            l.debug(
                'gasPrice', gasPrice.toString(),
                'userGas', userGas.toString(),
                'userGasCost', userGasAmount.toString(),
                'userAmount', userAmount.toString()
            );
            const userTx = await p.sendTransaction({
                from, to, gasPrice, gas: userGas, value: userAmount, data: payload.message
            });
            return {userTx};
        }

        let taxedAmount = balance;
        if (balance.gt(maxBalance)) {
            l.info('maximum balance reach, taxed amount: [%s] wei', maxBalance.toString());
            taxedAmount = maxBalance;
        }

        const feeNetAmount = taxedAmount.mul(feeRate);
        const feeGas = await p.estimateGas({
            from, to, gasPrice, value: feeNetAmount.toString()
        });
        const feeGasAmount = feeGas.mul(gasPrice);
        const feeGrossAmount = feeNetAmount.plus(feeGasAmount);

        l.debug(
            'gasPrice', gasPrice.toString(),
            'feeGas', feeGas.toString(),
            'netFeeAmount', feeNetAmount.toString(),
            'feeGasAmount', feeGasAmount.toString(),
            'grossFeeAmount', feeGrossAmount.toString()
        );

        const userGrossAmount = balance.minus(feeGrossAmount);
        const userGas = await p.estimateGas({
            from, to, gasPrice, value: userGrossAmount.toString()
        });
        const userGasAmount = userGas.mul(gasPrice);
        const userNetAmount = userGrossAmount.minus(userGasAmount);

        l.debug(
            'gasPrice', gasPrice.toString(),
            'userGrossAmount', userGrossAmount.toString(),
            'userGas', userGas.toString(),
            'userGasAmount', userGasAmount.toString(),
            'userNetAmount', userNetAmount.toString()
        );

        l.info('fee [%s] ether', p.api.util.fromWei(feeNetAmount, 'ether').toString());
        l.info('user [%s] ether', p.api.util.fromWei(userNetAmount, 'ether').toString());

        await p.importAccount(privateKey, password);

        await p.unlockAccount(from, password);
        const feeTx = await p.sendTransaction({
            from, to: coinbase, gasPrice, gas: feeGas, value: feeNetAmount, data: payload.message
        });

        await p.unlockAccount(from, password);
        const userTx = await p.sendTransaction({
            from, to, gasPrice, gas: userGas, value: userNetAmount.toString()
        });

        return {userTx, feeTx};
    } finally {
        try {
            await p.removeAccount(from, password);
        } catch (e) {
            logger().debug(e);
        }
    }
});
