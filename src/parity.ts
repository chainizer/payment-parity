import * as Api from '@parity/api';
import {BigNumber} from 'bignumber.js';
import {config} from './config';
import {logger} from '@chainizer/support-winston';

export interface Transaction {
    from: string | Buffer
    to: string | Buffer
    value: string | BigNumber
    gas?: string | BigNumber
    gasPrice?: string | BigNumber
    data?: string
}

export class Parity {

    constructor(public api) {
    }

    async getGasPrice(): Promise<BigNumber> {
        return await this.api.eth.gasPrice();
    }

    async estimateGas(tx: Transaction): Promise<BigNumber> {
        return await this.api.eth.estimateGas(tx);
    }

    async sendTransaction(tx: Transaction): Promise<string> {
        return await this.api.eth.sendTransaction(tx);
    }

    async getCoinbase(): Promise<string> {
        return await this.api.eth.coinbase();
    }

    async getBalance(address: string): Promise<BigNumber> {
        return await this.api.eth.getBalance(address);
    }

    async importAccount(privateKey: string, password: string): Promise<boolean> {
        return await this.api.parity.newAccountFromSecret(privateKey, password);
    }

    async unlockAccount(address: string, password: string): Promise<boolean> {
        return await this.api.personal.unlockAccount(address, password);
    }

    async removeAccount(address: string, password: string): Promise<boolean> {
        logger().debug('removeAccount', address, password);
        return await this.api.parity.killAccount(address, password);
    }

}

let instance: Parity;

export function parity(): Parity {
    if (!instance) {
        const provider = new Api.Provider.Http(
            config().parity.url,
            config().parity.pollingTimeout
        );
        const api = new Api(provider);
        instance = new Parity(api);
    }
    return instance;
}