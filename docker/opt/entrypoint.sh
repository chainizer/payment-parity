#!/usr/bin/env bash

# RABBIT

RABBIT_URL="amqp://"
if [ ! -z ${RABBIT_PASS} ]; then
    RABBIT_URL="${RABBIT_URL}${RABBIT_USER}:${RABBIT_PASS}@"
fi
RABBIT_URL="${RABBIT_URL}${RABBIT_HOST:-rabbit-discovery}/"

# START

ARGS="$@ --config /opt/config/${CURRENCY:-kovan}.config.json --set.cnq.amqp.connection ${RABBIT_URL} --set.parity.url ${PARITY_URL}"

exec npm start --production -- ${ARGS}
