#!/usr/bin/env bash

docker stop payment-eth-parity-kovan
docker run \
    --rm $1 \
    --publish 8545:8545 \
    --name payment-eth-parity-kovan \
    --volume payment-eth-parity-kovan:/root/.local/share/io.parity.ethereum \
    parity/parity:stable --chain kovan --rpcapi eth,personal,parity_accounts --rpcaddr 0.0.0.0 --rpcport 8555 -lrpc=trace --author 0xDe0e39D882a98FD88f2aEFf09D6126B5BFC176E4 --no-ui --no-dapps --no-ws --no-ipc --no-discovery --max-peers=10

