#!/usr/bin/env bash

docker stop payment-eth-parity-dev
docker run \
    --rm $1 \
    --publish 8545:8545 \
    --name payment-eth-parity-dev \
    parity/parity:stable --gasprice 0x74426cc --chain=dev --config=dev --geth --rpcapi eth,personal,parity_accounts --rpcaddr 0.0.0.0 --rpcport 8545 -lrpc=trace --no-ui --no-dapps
